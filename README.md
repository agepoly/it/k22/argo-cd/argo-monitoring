# argo-monitoring

- name: grafana
  version: "6.57.0"
  repository: "https://grafana.github.io/helm-charts"
- name: kube-prometheus-stack
  version: "48.2.0"
  repository: "https://prometheus-community.github.io/helm-charts"
- name: loki
  version: "5.6.1"
  repository: "https://grafana.github.io/helm-charts"
- name: promtail
  version: "6.11.2"
  repository: "https://grafana.github.io/helm-charts"
